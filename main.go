package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/usautoparts/ebay-api"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type timestamp struct {
	ID            bson.ObjectId `bson:"_id,omitempty"`
	APICall       string        `json:"apicall"`
	Modtimefrom   string        `json:"modtimefrom"`
	Modtimeto     string        `json:"modtimeto"`
	Starttimefrom string        `json:"starttimefrom"`
	Starttimeto   string        `json:"starttimeto"`
	Endtimefrom   string        `json:"endtimefrom"`
	Endtimeto     string        `json:"endtimeto"`
}

func main() {

	c := ebayapi.NewConfig()
	c.ShowDebug = true

	session, err := mgo.Dial(os.Getenv("DBHOST") + ":" + os.Getenv("DBPORT"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	db := session.DB(os.Getenv("DB")).C("gse_dump")
	ts := session.DB(os.Getenv("DB")).C("timestamp")

	/*
	** Get last previous run end time
	** from DB or file and set as start time
	 */
	selector := bson.M{"apicall": "GetSellerEvents"}
	timestamp := timestamp{}
	start := time.Time{}
	err = ts.Find(selector).One(&timestamp)

	if err != nil {
		// If no time start in DB, use current time - 30 minutes

		start = time.Now().UTC()
		start = start.Add(time.Minute * -30)

		/*
			log.Println("No start time in database, terminating program...")
			return
		*/
	} else {
		layout := "2006-01-02 15:04:05.000"
		split := strings.Split(timestamp.Modtimeto, " ")
		res, err := time.Parse(layout, split[0]+" "+split[1][0:12])
		if err != nil {
			panic(err)
		}
		start = res

		currentTime := time.Now().UTC()
		diffTime := currentTime.Sub(res)
		hour := time.Minute * 30

		if diffTime < hour {
			log.Println("Last run and current time is less than 30 minutes, terminating program...")
			return
		}
	}

	threshold := 3000
	interval := float64(30)
	multiplier := time.Duration(interval) * time.Minute

	for {

		modStart := start.Add(time.Minute * -2).String()
		modEnd := start.Add(multiplier).String()

		// modStart := "2016-09-15 05:59:36.261"
		// modEnd := "2016-09-15 06:10:36.261"

		arrStart := strings.Split(modStart, " ")
		arrEnd := strings.Split(modEnd, " ")

		handle := ebayapi.GetSellerEventsRequest{
			Xmlns:             "urn:ebay:apis:eBLBaseComponents",
			EBayAuthToken:     c.Token,
			ErrorLanguage:     "en_US",
			WarningLevel:      "High",
			UserID:            os.Getenv("SELLER"),
			DetailLevel:       "",
			HideVariations:    true,
			IncludeWatchCount: false,
			NewItemFilter:     true,
			ModTimeFrom:       arrStart[0] + "T" + arrStart[1],
			ModTimeTo:         arrEnd[0] + "T" + arrEnd[1],
			// StartTimeFrom: arrStart[0] + "T" + arrStart[1],
			// StartTimeTo:   arrEnd[0] + "T" + arrEnd[1],
			// EndTimeFrom: arrStart[0] + "T" + arrStart[1],
			// EndTimeTo:   arrEnd[0] + "T" + arrEnd[1],
		}

		response, err := handle.Fetch(*c)
		if err != nil {
			/* TODO
			** Log error to DB?
			 */
			panic(err)
		}

		if len(response.ItemArray) < threshold {

			if c.ShowDebug {
				output, err := xml.MarshalIndent(response, "  ", "    ")
				if err != nil {
					panic(err)
				}
				fmt.Printf("%s", output)
			}

			bulk := db.Bulk()

			for _, v := range response.ItemArray {

				v.Timestamp = response.Timestamp
				v.Ack = response.Ack
				v.Version = response.Version
				v.Build = response.Build
				v.TimeTo = response.TimeTo
				v.CorrelationID = response.CorrelationID
				v.HardExpirationWarning = response.HardExpirationWarning
				v.Errors = response.Errors
				v.Status = false

				bulk.Insert(v)

				/*
					err = db.Insert(v)
					if err != nil {
						panic(err)
					}
				*/

			}

			_, err = bulk.Run()
			if err != nil {
				panic(err)
			}

			/*
			** Save modstart and modend time to DB
			** to be used as start time for next run
			 */
			timestamp.APICall = "GetSellerEvents"
			timestamp.Modtimefrom = modStart
			timestamp.Modtimeto = modEnd

			_, err = ts.Upsert(selector, timestamp)
			if err != nil {
				/* TODO
				** Log error to DB?
				 */
				panic(err)
			}

			fmt.Println("Done GetSellerEvents API Call")

			break

		} else {
			multiplier = multiplier / 2
			continue
		}

	}

}
