FROM docker.io/golang:latest

#RUN git clone https://svnsync:5vnm1rr0r@gitlab.usautoparts.com/omp/job-queue-service.git
ADD . /go/src/gitlab.usautoparts.com/omp/get-seller-events

RUN cd /go/src/gitlab.usautoparts.com/omp/get-seller-events
RUN go install gitlab.usautoparts.com/omp/get-seller-events

ENTRYPOINT /go/bin/get-seller-events
